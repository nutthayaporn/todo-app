import { fetchApi } from "../utils";

const GetAllTodoApi = (token) => {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return fetchApi("todos", "GET", {}, headers);
};

const CreateTodoApi = (token, data) => {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return fetchApi("todos", "POST", JSON.stringify(data), headers);
};

const DeleteTodoApi = (token, id) => {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return fetchApi(`todos/${id}`, "DELETE", {}, headers);
};

const EditTodoApi = (token, data, id) => {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return fetchApi(`todos/${id}`, "PUT", JSON.stringify(data), headers);
};

export { GetAllTodoApi, CreateTodoApi, DeleteTodoApi, EditTodoApi };
