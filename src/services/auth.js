import { fetchApi } from "../utils";

const loginApi = ({ username, password }) => {
  const data = {
    username,
    password,
  };
  return fetchApi("users/auth", "POST", JSON.stringify(data));
};

export { loginApi };
