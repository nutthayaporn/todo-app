import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { Provider, useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import configureStore from "./store/configureStore";
import "antd/dist/antd.css";
import "./index.css";

import { TodolistPage, LoginPage } from "./pages";
import { GetAllTodoApi } from "./services/todoList";

import Styled from "./styles";

const store = configureStore();

const App = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const todoToken = localStorage.getItem("todoToken");

  const { authIsChecked, authIsLogin } = useSelector((state) => {
    return {
      authIsChecked: state.auth.isChecked,
      authIsLogin: state.auth.isLogin,
    };
  });

  useEffect(() => {
    if (!todoToken) {
      dispatch({ type: "AUTH_IS_CHECKED", isChecked: true });
      return;
    }

    const init = async () => {
      const todoResult = await GetAllTodoApi(todoToken);
      dispatch({ type: "AUTH_IS_CHECKED", isChecked: true });
      if (_.get(todoResult, "message")) {
        return;
      }
      dispatch({ type: "AUTH_IS_LOGIN", isLogin: true });
      history.push("/todolist");
    };

    init();
  }, [todoToken]);

  const PrivateRoute = ({ children, ...rest }) => {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          authIsLogin ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
              }}
            />
          )
        }
      />
    );
  };

  if (!authIsChecked) {
    return <div>Loading...</div>;
  }
  return (
    <Styled>
      <Switch>
        <PrivateRoute path="/todolist">
          <TodolistPage />
        </PrivateRoute>
        <Route path="/">
          <LoginPage />
        </Route>
      </Switch>
    </Styled>
  );
};

function AppProvider() {
  return (
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  );
}

export default AppProvider;
