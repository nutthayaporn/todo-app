const initialState = {
  isChecked: false,
  isLogin: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "AUTH_IS_CHECKED":
      return {
        ...state,
        isChecked: action.isChecked,
      };
    case "AUTH_IS_LOGIN":
      return {
        ...state,
        isLogin: action.isLogin,
      };
    default:
      return state;
  }
};
