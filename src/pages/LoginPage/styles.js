import styled from "styled-components";

const Styled = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  .ant-form-item-control-input-content {
    text-align: right;
  }
`;

export default Styled;
