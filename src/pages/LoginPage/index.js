import React from "react";
import { Form, Input, Button } from "antd";
import _ from "lodash";
import { useDispatch } from "react-redux";

import { loginApi } from "../../services/auth";

import Styled from "./styles";

const LoginPage = () => {
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    const username = _.get(values, "username");
    const password = _.get(values, "password");

    const loginResult = await loginApi({ username, password });
    const token = _.get(loginResult, "token");
    if (token) {
      localStorage.setItem("todoToken", token);
      dispatch({ type: "AUTH_IS_LOGIN", isLogin: true });
    }
  };

  return (
    <Styled>
      <Form name="basic" onFinish={onFinish}>
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Styled>
  );
};

export default LoginPage;
