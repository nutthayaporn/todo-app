import React, { useEffect, useState } from "react";
import { Button, Modal, Form, Input, Card, Popconfirm } from "antd";
import _ from "lodash";
import dayjs from "dayjs";

import {
  GetAllTodoApi,
  CreateTodoApi,
  DeleteTodoApi,
  EditTodoApi,
} from "../../services/todoList";

import { DeleteOutlined } from "@ant-design/icons";

import Styled from "./styles";

const { TextArea } = Input;

const TodolistPage = () => {
  const [todos, setTodos] = useState([]);
  const [openCrateTodo, setOpenCrateTodo] = useState(false);
  const [todoId, setTodoId] = useState(null);
  const [form] = Form.useForm();

  const todoToken = localStorage.getItem("todoToken");

  useEffect(() => {
    const init = async () => {
      const todoResult = await GetAllTodoApi(todoToken);
      if (!todoResult) {
        return;
      }
      setTodos(todoResult);
    };

    init();
  }, []);

  const handleClickDelete = async (id) => {
    const deleteResult = await DeleteTodoApi(todoToken, id);
    if (_.get(deleteResult, "message")) {
      return;
    }

    const newTodos = _.filter(todos, (todo) => {
      return _.get(todo, "_id") !== id;
    });

    setTodos(newTodos);
  };

  const onCreate = async (values) => {
    const data = {
      title: _.get(values, "title"),
      description: _.get(values, "description"),
    };

    const createTodoResult = await CreateTodoApi(todoToken, data);

    if (_.get(createTodoResult, "message")) {
      return;
    }

    const newTodos = [...todos, createTodoResult];

    form.resetFields();

    setTodos(newTodos);
    setOpenCrateTodo(false);
  };

  const onEdit = async (values) => {
    const newTodos = [...todos];

    const title = _.get(values, "title");
    const description = _.get(values, "description");

    const data = {
      title,
      description,
    };

    const editTodoResult = await EditTodoApi(todoToken, data, todoId);

    if (_.get(editTodoResult, "message")) {
      return;
    }

    const currentIndex = _.findIndex(todos, ["_id", todoId]);

    _.set(newTodos, `${currentIndex}.title`, title);
    _.set(newTodos, `${currentIndex}.description`, description);

    setTodos(newTodos);
    setTodoId(null);
  };

  return (
    <Styled>
      <div className="button-block">
        <Button
          type="primary"
          size="large"
          onClick={() => setOpenCrateTodo(true)}
        >
          Create A To Do
        </Button>
      </div>
      <div>
        {_.isEmpty(todos) ? (
          <div>Empty! Click "Create A To Do" button to add some</div>
        ) : (
          <div>
            {_.map(todos, (todo, index) => {
              const id = _.get(todo, "_id");
              const title = _.get(todo, "title");
              const description = _.get(todo, "description");
              const date = dayjs(_.get(todo, "updatedAt")).format("DD-MM-YYYY");
              return (
                <div className="card-block" key={index}>
                  <div
                    onClick={() => {
                      setTodoId(id);
                      const currentIndex = _.findIndex(todos, ["_id", id]);
                      const initialValues = {
                        title: _.get(todos, `${currentIndex}.title`),
                        description: _.get(
                          todos,
                          `${currentIndex}.description`
                        ),
                      };
                      form.setFieldsValue(initialValues);
                    }}
                  >
                    <Card title={title} style={{ width: 300 }} size="small">
                      <p className="description">{description}</p>
                      <p className="date">{date}</p>
                    </Card>
                  </div>
                  <div className="button-delete">
                    <Popconfirm
                      title="Are you sure delete this task?"
                      onConfirm={() => handleClickDelete(id)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <DeleteOutlined />
                    </Popconfirm>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>

      <Modal
        visible={openCrateTodo}
        title="Create a new to do"
        okText="Create"
        cancelText="Cancel"
        onCancel={() => setOpenCrateTodo(false)}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              onCreate(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
      >
        <Form form={form} layout="vertical" name="form_create">
          <Form.Item
            name="title"
            label="Title"
            rules={[
              {
                required: true,
                message: "Please input the title of collection!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <TextArea autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        visible={todoId}
        title="Edit your to do"
        okText="Edit"
        cancelText="Cancel"
        onCancel={() => setTodoId(null)}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              onEdit(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
        destroyOnClose
        afterClose={() => form.resetFields()}
      >
        <Form form={form} layout="vertical" name="form_edit">
          <Form.Item
            name="title"
            label="Title"
            rules={[
              {
                required: true,
                message: "Please input the title of collection!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <TextArea autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
        </Form>
      </Modal>
    </Styled>
  );
};

export default TodolistPage;
