import styled from "styled-components";

const Styled = styled.div`
  width: 100%;
  min-height: 100%;
  padding: 50px 0;
  background-color: #64c4b4;
  display: flex;
  align-items: center;
  flex-direction: column;

  .button-block {
    margin-bottom: 30px;
  }
  .card-block {
    position: relative;
    .ant-card {
      margin-bottom: 5px;
      .description{
        word-break: break-word;
      }
      .date {
        position: absolute;
        bottom: -9px;
        right: 5px;
        font-size: 12px;
      }
    }
    .button-delete {
      position: absolute;
      top: 7px;
      right: 8px;
    }
  }

  .ant-btn-primary {
    background-color: #2a2d40;
    border-color: #2a2d40;
  }
`;

export default Styled;
