const fetchApi = (url, method = "GET", data = {}, headers = {}) => {
  const options = {
    method,
    headers: {
      ...headers,
    },
  };
  if (method === "POST" || method === "PUT") {
    if (headers !== false) {
      options.headers = {
        "Content-Type": "application/json",
        ...options.headers,
      };
    }
    options.body = data;
  }

  return fetch(`https://candidate.neversitup.com/todo/${url}`, options)
    .then((response) => response.json())
    .catch((error) => {
      return error;
    });
};

export { fetchApi };
